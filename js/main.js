var waterReminderObj = {

};

var drink = function(){
	this.doc 					= document;
	this.imperialConstant		= 0.5;
	this.metricConstant			= 33;
	this.hotDay					= false;
	this.sports					= false;
	this.adjustments			= 0;
	this.drinkAmount			= 0;
	this.startDate				= null;
	this.initOk					= this.doc.getElementById("initOk");
	this.weightField			= this.doc.getElementById("weight");
	this.measureSystem			= this.doc.getElementById("measure");
};

var utils = function(){
	this.today 					= new Date();
    this.doc 					= document;
	this.curNavDate				= this.doc.getElementById("curNavDate");
	this.monthNames 			= [ "January", "February", "March", "April", "May", "June",
    								"July", "August", "September", "October", "November", "December" ];
    this.loadingElem			= this.doc.getElementById("loadingDialog");
    this.initDialog				= this.doc.getElementById("initDialog");
    this.contentGrid			= this.doc.getElementById("content");

};

//----------------------------------------------------------Initialize function----------------------------------------------------------
var init = function () {
	
	var utilsObj 	= new utils();
	var drinkObj	= new drink();
	utilsObj.init();
	drinkObj.init(utilsObj);
	console.log("init() called");
	
	// add eventListener for tizenhwkey
	document.addEventListener('tizenhwkey', function(e) {
		if(e.keyName == "back") {
			try {
				tizen.application.getCurrentApplication().exit();
			} catch (error) {
				console.error("getCurrentApplication(): " + error.message);
			}
		}
	});
};
document.body.onload = init;

//----------------------------------------------------------Utils prototype----------------------------------------------------------
utils.prototype.init = function() {
	this.setCurNavDate();
	this.debugGrid();
};

utils.prototype.addChangeListener = function(obj) {
	obj.addEventListener("change", function (e) {
		console.log(e);
		console.log(obj);
		obj.setAttribute("bindAttr", obj.value);
	})
};

utils.prototype.debugGrid = function() {
	try {
		var temp = [];
		temp = document.getElementsByClassName("grid");
		for (var i = temp.length - 1; i >= 0; i--) {
			var color = '#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6);
        	// temp[i].style.width = (temp[i].getBoundingClientRect().width - 2) + "px";
        	// temp[i].style.border = "1px solid "+ color;
        	// temp[i].style.background = color;
		};
    } catch(e){
      console.log(e.message);
    }
};

utils.prototype.setCurNavDate = function() {
	var day = this.today.getDate();
	var month = this.monthNames[this.today.getMonth()];
	var year = this.today.getFullYear();
	var str = day + "-" + month + "-" + year;
	this.curNavDate.innerHTML = str;
};

utils.prototype.startTime = function() {
	
	var h = this.today.getHours();
	var m = this.today.getMinutes();
	var s = this.today.getSeconds();
	m = checkTime(m);
	s = checkTime(s);
	document.getElementById('divbutton1').innerHTML="Current time: " + h + ":" + m + ":" + s;
	var t = setTimeout(this.startTime, 250);
};

utils.prototype.checkTime = function() {
	if (i < 10) {
		i="0" + i;
	}
	return i;
};

utils.prototype.postNotification = function(message) {
		try {
		    var appControl = new tizen.ApplicationControl(
			               "http://tizen.org/appcontrol/operation/create_content",
			               null,
			               null,
			               null);
			var notificationDict = {
			  		  soundPath : "music/Over the horizon.mp3",	//relative path to sound file
			          content : (message),
			          vibration : true, 
			          appControl : appControl};

// StatusNotification(StatusNotificationType statusType, DOMString title, optional StatusNotificationInit? notificationInitDict);
			var notification = new tizen.StatusNotification("SIMPLE", 
			          "Device API", notificationDict);
			               
			tizen.notification.post(notification);
	 } catch (err) {
		 console.log (err.name + ": " + err.message);
	 }
};

utils.prototype.loading = function(cmd) {
	switch(cmd){
		case "start"  		:
		case "loading"		: this.loadingElem.style.display = "block";
			break;
		case "end"			: this.loadingElem.style.display = "none";
			break;
	}
};

utils.prototype.modalSwitch = function(cmd) {
	switch(cmd){
		case "initDialog"		: this.initDialog.style.display = "block";
			this.loading("end");
			break;
		case "contentGrid"		: this.contentGrid.style.display = "block";
			this.initDialog.style.display = "none";
			this.loading("end");
			break;
	}
};

//----------------------------------------------------------Drink prototype----------------------------------------------------------
drink.prototype.init = function(utilsObj) {
	this.utilsObj = utilsObj;
	this.initOk.addEventListener("touchend", this.startReminder.bind(this));
	this.utilsObj.addChangeListener(this.weightField);

	var temp = localStorage.getItem("waterReminderObj");
	if(temp) {
		waterReminderObj = JSON.parse(temp);
		this.utils.modalSwitch("contentGrid");
	}
	else
		this.utilsObj.modalSwitch("initDialog");
};

drink.prototype.method_name = function() {
	this.utils.loading("end");
	this.initOk.style.display = "block";
};

drink.prototype.startReminder = function(e) {
	console.log(this);
};

drink.prototype.startAlarm = function() {
	// var alarm = new tizen.AlarmAbsolute(new Date(2014, 10, 4, 8, 0));

	// Sets an alarm in 1 minute from now
	var alarm1 = new tizen.AlarmRelative(1 * tizen.alarm.PERIOD_HOUR);
	// tizen.alarm.add(alarm1, appId);

	// Sets an alarm in one hour, recurring after every 2 minutes
	// var alarm2 = new tizen.AlarmRelative(tizen.alarm.PERIOD_HOUR, 2 * tizen.alarm.PERIOD_MINUTE);
	// tizen.alarm.add(alarm2, appId);

	var appControl = new tizen.ApplicationControl(
			               "http://tizen.org/appcontrol/operation/view",
			               "alarm",
			               "alarm/Drink",
			               null);
	tizen.alarm.add(alarm1, tizen.application.getCurrentApplication().appInfo.id, appControl);
	var statusAlarm = tizen.alarm.get(alarm1.id);
	// Gets an alarm
	// var alarm = tizen.alarm.get(alarm.id);
	// console.log("The alarm will trigger at " + alarm.getNextScheduledDate());
};

drink.prototype.calculate = function(weight) {
	
};

//----------------------------------------------------------Modal manager----------------------------------------------------------
